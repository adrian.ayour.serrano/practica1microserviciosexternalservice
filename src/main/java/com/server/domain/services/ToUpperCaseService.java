package com.server.domain.services;

import com.server.infrastructure.proto.ToUpperCaseRequest;
import com.server.infrastructure.proto.ToUpperCaseResponse;
import com.server.infrastructure.proto.ToUpperCaseServiceGrpc;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class ToUpperCaseService extends ToUpperCaseServiceGrpc.ToUpperCaseServiceImplBase{

	public void toUpperCase(ToUpperCaseRequest request, StreamObserver<ToUpperCaseResponse> responseObserver) {
		
		ToUpperCaseResponse response = ToUpperCaseResponse.newBuilder().setMessage(request.getMessage().toUpperCase()).build();
		
		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}
}

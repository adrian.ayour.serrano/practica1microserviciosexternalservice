package com.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Practica1MicroserviciosExternalServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(Practica1MicroserviciosExternalServiceApplication.class, args);
	}

}
